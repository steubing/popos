" ~/.vimrc
"         _
"  __   _(_)_ __ ___  _ __ ___
"  \ \ / / | '_ ` _ \| '__/ __|
"   \ V /| | | | | | | | | (__
"  (_)_/ |_|_| |_| |_|_|  \___|
"
" Scott Steubing

" :source ~/.vimrc

" Bringing in defaults.vim ...
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

" Autoload plug.vim if it's not already installed ...
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Registering vimwiki in ~/Dropbox/vimwiki
" put Vimwiki in my MEGA folder; autogenerate the diary index
"let g:vimwiki_list = [{'path': '~/Dropbox/vimwiki/', 'auto_diary_index': 1}]

" Plugins, vim-plug required
call plug#begin('~/.vim/plugged')
Plug 'vimwiki/vimwiki' " vimwiki
Plug 'csexton/trailertrash.vim'
Plug 'mattn/calendar-vim'
Plug 'ap/vim-css-color'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentLine'
"Plug 'reedes/vim-pencil'
Plug 'machakann/vim-highlightedyank'
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

" Required for vim-plug
set nocompatible
filetype plugin on
syntax on

set background=dark

set number
set relativenumber
set shiftwidth=4
set tabstop=4 softtabstop=4
set expandtab
set smartindent
set nowrap
set ignorecase
set smartcase
set incsearch
set confirm
set showmatch
set matchpairs=(:),\[:\],{:},<:>
set linebreak
set sidescroll=6
set listchars+=precedes:<,extends:>
"set textwidth=80

set colorcolumn=80
highlight ColorColumn ctermbg=17 guibg=lightgrey

"set cursorcolumn
set cursorline

set splitbelow splitright

colorscheme dracula
"colorscheme solarized

" TrailerTrash color
hi UnwantedTrailerTrash guibg=LightBlue ctermbg=LightBlue

let g:airline_theme='simple'
let g:markdown_fenced_languages = ['html', 'python', 'ruby', 'vim', 'bash']

" for rofi
au BufNewFile,BufRead /*.rasi setf css

" mappings
map q <nop>

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Templates
autocmd BufNewFile *.sh 0r ~/Templates/bash.sh
autocmd BufNewFile *.desktop 0r ~/Templates/desktop.desktop


#!/bin/bash
#            _     _                     _
#   ___  ___| |__ | | ___   _ _ __   ___| |__
#  / _ \/ __| '_ \| |/ / | | | '_ \ / __| '_ \
# |  __/\__ \ |_) |   <| |_| | |_) |\__ \ | | |
#  \___||___/_.__/|_|\_\\__,_| .__(_)___/_| |_|
#                            |_|

# Scott Steubing
# backups up Documents and Downloads to external hard drive.

echo Backing up Data ...

rsync -av --delete ~/Dropbox/Documents /media/scott/TOSHIBA\ EXT
rsync -av --delete ~/Downloads /media/scott/TOSHIBA\ EXT
rsync -av --delete ~/Music /media/scott/TOSHIBA\ EXT
rsync -av --delete ~/Pictures /media/scott/TOSHIBA\ EXT
rsync -av --delete ~/Videos /media/scott/TOSHIBA\ EXT
echo ===========================================================================

sync
echo Backups Finished.


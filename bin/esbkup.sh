#!/bin/bash
#            _     _                     _
#   ___  ___| |__ | | ___   _ _ __   ___| |__
#  / _ \/ __| '_ \| |/ / | | | '_ \ / __| '_ \
# |  __/\__ \ |_) |   <| |_| | |_) |\__ \ | | |
#  \___||___/_.__/|_|\_\\__,_| .__(_)___/_| |_|
#                            |_|

# Scott Steubing
# backups up Documents and Downloads to external hard drive.

echo Backing up Data ...

rsync -av --delete ~/Dropbox/Documents /media/scott/easystore/Scott
rsync -av --delete ~/Downloads /media/scott/easystore/Scott
#rsync -av --delete ~/Dropbox /media/scott/easystore/Scott
rsync -av --delete ~/Pictures /media/scott/easystore/Scott
rsync -av --delete ~/Videos /media/scott/easystore/Scott
#rsync -a --delete ~/pCloudDrive /media/scott/easystore/Scott
echo ===========================================================================

sync
echo Backups Finished.


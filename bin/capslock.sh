#!/bin/bash

xset q | grep Caps | cut -c 22-24 | tr "[:lower:]" "[:upper:]"

#!/bin/bash
#
#      _                                   _
#  ___| | ___  __ _ _ __  _   _ _ __   ___| |__
# / __| |/ _ \/ _` | '_ \| | | | '_ \ / __| '_ \
#| (__| |  __/ (_| | | | | |_| | |_) |\__ \ | | |
# \___|_|\___|\__,_|_| |_|\__,_| .__(_)___/_| |_|
#                              |_|

# Scott Steubing
# cleans up apt, flatpak, & snap

echo 'Clean Up'

echo 'flatpak remove --unused'
flatpak remove --unused

echo 'Cleaning up Dropbox'
rm -R ~/Dropbox/.dropbox.cache/*

echo 'Cleaning up vifm'
rm -R ~/.local/share/vifm/Trash/*
rm -R ~/ExtraDrive/.vifm-Trash-1000/*

#echo 'Removing screenshots'
#rm ~/Pictures/Screenshot*

echo 'Cleaning up thumbnail cache'
rm -R ~/.cache/thumbnails/*

echo 'All done!'

